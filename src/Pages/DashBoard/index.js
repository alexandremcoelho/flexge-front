import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import Contracts from "./Contracts";

const DashBoard = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const pass = useSelector((state) => state)
    useEffect(() => {
        if(pass.user.isLogged !== true){
            navigate("/")
        }
    }, []);

    return <Contracts />
}

export default DashBoard