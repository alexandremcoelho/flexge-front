import styled from "styled-components";

export const HeaderDiv = styled.div`
display: flex;
justify-content: space-between;
padding: 29px 50px;
background-color: #eee;
`;


export const OutsideDiv = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`;

export const InsideDiv = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
width: 90%;
`;