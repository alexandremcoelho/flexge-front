import { Space, Table } from 'antd';
import React from 'react';
import api from '../../../../services/api';

function dell(item){
    api
    .delete(`/contract/${item._id}/`)
    .then((res) => {
        window.location.reload()
    })
    .catch((error) => {
      console.log(error);
    });
    

}


const columns = [
{
    title: 'Document Number',
    dataIndex: 'document',
},
{
    title: 'social_reason',
    dataIndex: 'social_reason',
},
{
    title: 'Company',
    dataIndex: 'company',
    render: (item) => <a>{item.name}</a>,
},
{
    title: 'Action',
    key: 'action',
    render: (_, record) => (
      <Space size="middle">
        <button onClick={(i) => dell(_)}>Delete</button>
      </Space>
    ),
  },
];

const onChange = (pagination, filters, sorter, extra) => {
  console.log('params', pagination, filters, sorter, extra);
};
const ContractTable = ({Data, setListContracts}) => {

return <Table columns={columns} dataSource={Data} onChange={onChange} />;
}
export default ContractTable;