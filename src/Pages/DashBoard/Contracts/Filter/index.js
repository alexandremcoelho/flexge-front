import { Form, Input } from 'antd';
import React from 'react';
import api from '../../../../services/api';

const Filters = ({setListContracts}) => {
    const [form] = Form.useForm();

    const filter = (values) => {
        const url = `/Contract?document=${values.document}&social_reason=${values.social_reason}&company=${values.company}`
        
        api
          .get(url,)
          .then((response) => {
            setListContracts(response.data);
            console.log(response)
          })
          .catch((e) => console.log(e));
      };

    return (
        <Form
        form={form}
        layout="inline"
        onFinish={filter}
        style={{flexWrap: "nowrap"}}
        >
          <Form.Item label="Document" name="document">
            <Input />
          </Form.Item>
          <Form.Item label="Social Reason" name="social_reason">
            <Input />
          </Form.Item>
          <Form.Item label="Company" name="company">
            <Input />
          </Form.Item>
          <Form.Item >
            <button type="submit">search</button>
          </Form.Item>
        </Form>
      );
}

export default Filters 