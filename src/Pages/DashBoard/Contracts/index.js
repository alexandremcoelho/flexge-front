import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router";
import api from "../../../services/api";
import Filters from './Filter';
import { HeaderDiv, InsideDiv, OutsideDiv } from './style';
import ContractTable from './Table';

const Contracts = () => {
    const [ListContracts, setListContracts] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        api
          .get('/Contract',)
          .then((response) => {
            setListContracts(response.data);
          })
          .catch((e) => console.log(e));
    }, []);

    return (
    <OutsideDiv >
        <InsideDiv>
            <HeaderDiv>Contracts <button onClick={() => navigate("/contract")}>Create Contract</button></HeaderDiv>
            <Filters setListContracts={setListContracts} />
            <ContractTable Data={ListContracts} setListContracts={setListContracts}/>
        </InsideDiv>
    </OutsideDiv>
    )
}

export default Contracts