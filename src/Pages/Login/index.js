import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as yup from "yup";
// import { useToken } from "../../Providers/Token";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { login } from '../../Providers/UserSlice';
import api from "../../services/api";
import {
    Box, Container,
    Header, Input,
    LoginButton,
    LoginFormContainer, TextInput
} from "./style";

const Login = ({ userType, notifyError, notifyLogin, notifyLoginArea }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch()

  const schema = yup.object().shape({
    name: yup.string().required("Campo Obrigatório"),
    password: yup
      .string()
      .required("Campo Obrigatório")
      .min(5, "Mínimo de 5 caracteres"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const redirectToDashboard = () => {
    navigate("/dashboard")
  };

  const onSubmit = (data) => {
    api
      .post("Login", data)
      .then((response) => {
        dispatch(login(data.name))
        redirectToDashboard();
        notifyLogin();
        reset();
      })
      .catch((error) => {
        console.log(data)
        notifyError();
      });
  };

  return (
    <LoginFormContainer>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Container>
          <Header>Login</Header>
          <Box>
            <TextInput error={!!errors.name}>Usuario:</TextInput>
            <Input {...register("name")} error={!!errors.name} />
            <p>{errors.name?.message}</p>
          </Box>
          <Box>
            <TextInput error={!!errors.password}>Senha:</TextInput>
            <Input
              {...register("password")}
              type="password"
              error={!!errors.password}
            />
            <p>{errors.password?.message}</p>
          </Box>
          <LoginButton type="submit">Entrar</LoginButton>
        </Container>
      </form>
    </LoginFormContainer>
  );
};

export default Login;

