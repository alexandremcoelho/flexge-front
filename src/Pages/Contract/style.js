import styled from "styled-components";

export const FormContainer = styled.div`
  min-width: 80vw;
  border-radius: 16px;

  @media (min-width: 768px) {
    min-width: 300px;
  }
`;

export const HeaderDiv = styled.div`
display: flex;
justify-content: space-between;
padding: 29px 50px;
background-color: #eee;
`;

export const ContractPage = styled.div`
width: 90%;
margin-top: 20px;
`;

export const Page = styled.div`
display: flex;
justify-content: center;
`;

export const Button = styled.button`
width: 100px
`;
