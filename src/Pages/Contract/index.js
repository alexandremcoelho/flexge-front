import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import CreateContract from './CreatContract';
import { Button, ContractPage, HeaderDiv, Page } from "./style";

const Contract = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const pass = useSelector((state) => state)
    useEffect(() => {
        if(pass.user.isLogged !== true){
            navigate("/")
        }
    }, []);
    
    return (<Page>
            <ContractPage>
                <HeaderDiv>Create Contract <Button onClick={() => navigate("/dashboard")}>Back</Button></HeaderDiv>
                <CreateContract />
            </ContractPage>
        </Page>)
}

export default Contract