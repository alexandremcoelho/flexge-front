import styled from "styled-components";

export const ContractForm = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;

  h5 {
    color: #380f52;
    margin: 12px 0;
  }

  span {
    color: #0ec977;
    cursor: pointer;
  }
`;

export const FlexRowDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;