import { DatePicker, Form, Input, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router";
import api from "../../../services/api";
import Product from './Products';
import { FlexRowDiv } from './style';


const CreateContract = () => {
    const [form] = Form.useForm();
    const [Countries, setCountries] = useState([]);
    const [Company, setCompany] = useState([]);
    const [States, setStates] = useState([]);
    const [Products, setProducts] = useState([]);
    const navigate = useNavigate();


  useEffect(() => {
      api
        .get('/Country',)
        .then((response) => {
            setCountries(response.data);
        })
        .catch((e) => console.log(e));
        api
        .get('/Company',)
        .then((response) => {
            setCompany(response.data);
        })
        .catch((e) => console.log(e));
  }, []);

  const countryChange = (value) => {
    setStates(Countries.find((item) => item._id === value).state)
  };

  const onFinish = (values) => {
    if(values.start){
    let data = values.start._d
    values.start = ((data.getDate() )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
  }
  if(values.end){
    let data = values.end._d
    values.end = ((data.getDate() )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
  }
  if(values.end){
    let data = values.due_day._d
    values.due_day = ((data.getDate() )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
  }
    values.products = Products

    api
    .post("/Contract", values)
    .then((res) => {
      navigate("/dashboard")
    })
    .catch((error) => {
      console.log(error);
    });
  };
    
    return (<>
        <Form
        form={form}
        layout="vertical"
        onFinish={onFinish}
      >
        <FlexRowDiv>
        <Form.Item label="country" 
        name="country"
        rules={[
          {
            required: true,
            message: '',
          },
        ]}>
          <Select onChange={countryChange}>
            {
                Countries.map((value,key)=>
                <Select.Option value={value._id} >{value.name}</Select.Option>
                )
            }
          </Select>
        </Form.Item>
        <Form.Item label="State"
         name="state"
         rules={[
          {
            required: true,
            message: '',
          },
        ]}>
          <Select>
          {
                States.map((value,key)=>
                <Select.Option value={value._id} >{value.name}</Select.Option>
                )
            }
          </Select>
        </Form.Item>
        <Form.Item
          label="City"
          name="city"
        >
          <Input />
        </Form.Item>
        </FlexRowDiv>
        <FlexRowDiv>
        <Form.Item
          label="Document Number"
          name="document"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}>
          <Input />
        </Form.Item>

        <Form.Item
          label="Social Reason"
          name="social_reason"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}>
          <Input />
        </Form.Item>
        </FlexRowDiv>
        <FlexRowDiv>
        <Form.Item
          label="Address"
          name="address"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="District"
          name="district"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}>
          <Input />
        </Form.Item>

        <Form.Item
          label="Number"
          name="number"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}>
          <Input />
        </Form.Item>
        </FlexRowDiv>
        <FlexRowDiv>
        <Form.Item
          label="Zip Code"
          name="zip_code"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}>
          <Input />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}>
          <Input />
        </Form.Item>

        <Form.Item
          label="Phone"
          name="phone"
        >
          <Input />
        </Form.Item>
        </FlexRowDiv>

        <FlexRowDiv>
        <Form.Item label="Contracts starts in" name="start"
        rules={[
          {
            required: true,
            message: '',
          },
        ]}>
        <DatePicker />
        </Form.Item>

        <Form.Item label="Contracts ends in" name="end"
        rules={[
          {
            required: true,
            message: '',
          },
        ]}>
        <DatePicker />
        </Form.Item>

        <Form.Item label="Due day" name="due_day"
        rules={[
          {
            required: true,
            message: '',
          },
        ]}>
        <DatePicker />
        </Form.Item>
        </FlexRowDiv>
        <Form.Item label="Select a company" 
        name="company"
        rules={[
          {
            required: true,
            message: '',
          },
        ]}>
          <Select>
            {
                Company.map((value,key)=>
                <Select.Option value={value._id} >{value.name}</Select.Option>
                )
            }
          </Select>
        </Form.Item>

        <Form.Item>
          <button type="submit">Submit</button>
        </Form.Item>
      </Form>
      <Product Products={Products} setProducts={setProducts} />
      </>
        )
}

export default CreateContract