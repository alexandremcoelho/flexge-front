import { DatePicker, Form, Input, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import api from "../../../../services/api";
import { FlexRowDiv } from '../style';
import ProductTable from './Table';

const Product = ({Products, setProducts}) => {
    const [ListProducts, setListProducts] = useState([]);
    const [form] = Form.useForm();


    useEffect(() => {
        api
          .get('/Product',)
          .then((response) => {
            setListProducts(response.data);
          })
          .catch((e) => console.log(e));
    }, []);

    const onFinish = (values) => {
      if(values.date){
      let data = values.date._d
      values.beginning = ((data.getDate() )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
      }
      
      setProducts([...Products,  values])
    };

    return (
      <>
      <FlexRowDiv>
        <Form
        form={form}
        layout="inline"
        onFinish={onFinish}
      >
        <Form.Item label=" Product" name="name"
        rules={[
          {
            required: true,
            message: '',
          },
        ]}>
          <Select>
            {
                ListProducts.map((value,key)=>
                <Select.Option value={value.name} >{value.name}</Select.Option>
                )
            }
          </Select>
        </Form.Item>
        <Form.Item
          name="amount"
          label="Amount"
        >
          <Input />
        </Form.Item>
        
        <Form.Item
          name="price"
          label=" Final Price (R$)"
          rules={[
            {
              required: true,
              message: '',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Installments"
          name ="installments"
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Paid Installments"
          name="paid"
        >
          <Input />
        </Form.Item>

        <Form.Item label="Beginning of Term" name="date">
        <DatePicker />
        </Form.Item>

        <Form.Item>
          <button type="submit">+ add</button>
        </Form.Item>
      </Form>
      </FlexRowDiv>
      <ProductTable Data={Products} />
      </>
        )
}

export default Product