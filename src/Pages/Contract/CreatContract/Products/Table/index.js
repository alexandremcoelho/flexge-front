import { Table } from 'antd';
import React from 'react';
const columns = [
{
    title: 'Produc',
    dataIndex: 'name',
},
{
text: 'Amount',
value: 'amount',
},
{
    title: 'Price',
    dataIndex: 'price',
},
{
title: 'Installments',
dataIndex: 'installments',
},
{
    title: 'Paid Intallments',
    dataIndex: 'paid',
},
{
    title: 'Beginning of Term',
    dataIndex: 'beginning',
},

];

const onChange = (pagination, filters, sorter, extra) => {
  console.log('params', pagination, filters, sorter, extra);
};
const ProductTable = ({Data}) => <Table columns={columns} dataSource={Data} onChange={onChange} />;
export default ProductTable;