import { createSlice } from "@reduxjs/toolkit";

export const Slice = createSlice({
    name: 'user',
    initialState: {
        user: '',
        isLogged: false,
    },
    reducers: {
        login(state, { payload }){
            return {...state, isLogged: true, user: payload}
        },
        logout(state){
            return {...state, isLogged: false, user: ''}
        }
    }
})

export const { login, logout } = Slice.actions

export const selectUser = state => state.user

export default Slice.reducer