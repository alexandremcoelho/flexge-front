import React from "react";
import { Route, Routes } from "react-router-dom";
import Contract from "../Pages/Contract";
import DashBoard from "../Pages/DashBoard";
import Login from "../Pages/Login";

const Router = () => {
  return (
    <Routes>
      {/* <Route path="/" element={<Home />} /> */}
      <Route path="/" element={<Login />} />
      <Route path="/dashboard" element={<DashBoard />} />
      <Route path="/contract" element={<Contract />} />
    </Routes>

  );
};

export default Router;
